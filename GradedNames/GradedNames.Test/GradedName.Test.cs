﻿using NUnit.Framework;
using GradedNames;
using System.Collections.Generic;

namespace GradedNames.Test
{
    [TestFixture]
    public class GradedNameTest
    {
        GradedName _gradedName;

        [SetUp]
        public void Setup()
        {
            _gradedName = new GradedName();
        }

        [Test, TestCaseSource("GradedNameTestCases1")]
        public void Sort_ListOfGradedNamesInput_ReturnSortedListOfGradedNames(IList<GradedName> inputList, IList<GradedName> outputList)
        {
            var actualValue = _gradedName.Sort(inputList);
            Assert.AreEqual(actualValue.Count, outputList.Count, 
                $"The expected number of GradedName is {outputList.Count} but the actual number is {actualValue.Count}");
            for(int index=0; index < actualValue.Count; index++)
            {
                Assert.AreEqual(actualValue[index].FirstName, outputList[index].FirstName);
                Assert.AreEqual(actualValue[index].LastName, outputList[index].LastName);
                Assert.AreEqual(actualValue[index].Score, outputList[index].Score);
            }
        }

        [TearDown]
        public void TearDown()
        {
            _gradedName = null;
        }

        private IEnumerable<TestCaseData> GradedNameTestCases1()
        {
            yield return new TestCaseData(
                                  new List<GradedName>
                                  {
                                    new GradedName() { FirstName = "BUNDY", LastName = "TERESSA", Score= 88 },
                                    new GradedName() { FirstName = "SMITH", LastName = "ALLAN", Score= 70 },
                                    new GradedName() { FirstName = "KING", LastName = "MADISON", Score= 88 },
                                    new GradedName() { FirstName = "SMITH", LastName = "FRANCIS", Score= 85 },
                                  }
                                  ,
                                  new List<GradedName>
                                  {
                                    new GradedName() { FirstName = "BUNDY", LastName = "TERESSA", Score= 88 },
                                    new GradedName() { FirstName = "KING", LastName = "MADISON", Score= 88 },
                                    new GradedName() { FirstName = "SMITH", LastName = "FRANCIS", Score= 85 },
                                    new GradedName() { FirstName = "SMITH", LastName = "ALLAN", Score= 70 },
                                  }
                                );

        }
    }
}
