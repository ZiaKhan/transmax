﻿using System.Collections.Generic;
using System.Linq;

namespace GradedNames
{
    public class GradedName
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Score { get; set; }

        public IList<GradedName> Sort(IList<GradedName> gradedNames)
        {
            return gradedNames.OrderByDescending(x => x.Score).ThenByDescending(x => x.LastName).ThenBy(x => x.FirstName).ToList();
        }
    }
}
