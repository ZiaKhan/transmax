﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

using System.Text;
using System.IO;

namespace GradedNames
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("There should be one argument only. The full name of the input file!");
                return;
            }

            //check for valid paths
            string inputFilePath = args[0];
            if (!System.IO.File.Exists(inputFilePath))
            {
                Console.WriteLine($"The file {inputFilePath} doesn't exists.");
                return;
            }

            ProcessInputFile(inputFilePath);

        }


        private static void ProcessInputFile(string fileName)
        {
            System.IO.TextReader inputReader = System.IO.File.OpenText(fileName);

            List<GradedName> gradedNames = new List<GradedName>();
            string line;//name;
            string[] columns;
            while ((line = inputReader.ReadLine()) != null)
            {
                columns = line.Split(',');
                gradedNames.Add(new GradedName()
                {
                    FirstName = columns[0].Trim(),
                    LastName = columns[1].Trim(),
                    Score = Convert.ToInt32(columns[2].Trim())
                });
            }

            gradedNames = gradedNames.OrderByDescending(x => x.Score).ThenByDescending(x => x.LastName).ThenBy(x => x.FirstName).ToList();

            //output file
            string outputFile1Directory = Path.GetDirectoryName(Path.GetFullPath(fileName));
            string inputFileNameWithoutExt = Path.GetFileNameWithoutExtension(fileName);
            string outputFilePath = Path.Combine(outputFile1Directory, inputFileNameWithoutExt+ "-graded.txt");
            StreamWriter outputWriter = new StreamWriter(outputFilePath, false, Encoding.ASCII);
            foreach(GradedName name in gradedNames)
            {
                outputWriter.WriteLine(name.FirstName + ", " + name.LastName + ", " + name.Score);
            }

            outputWriter.Close();
        }

    }
}
